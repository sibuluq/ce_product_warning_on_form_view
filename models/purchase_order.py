# This file is part of Product Warning On Form View
#
# Copyright (C) 2023 CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from odoo import _, api, fields, models

class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    warning = fields.Text(compute="_compute_warning", string=_("Warning when purchase"))

    @api.depends('product_id')
    def _compute_warning(self):
        for record in self:
            if not record.product_id or not record.env.user.has_group('purchase.group_warning_purchase'):
                record.warning = False

            product_info = record.product_id

            if product_info.purchase_line_warn != 'no-message':
                record.warning = product_info.purchase_line_warn_msg
            else:
                record.warning = False
