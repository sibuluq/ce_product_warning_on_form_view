# This file is part of Product Warning On Form View
#
# Copyright (C) 2023 CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

{
    "name": "Product warning on form view",
    "version": "15.0.1.0.0",
    "category": "Extra Tools",
    "website": "https://gitlab.com/jvmdev/odoo/15/ce_product_warning_on_form_view",
    "author": "CV Java Multi Mandiri",
    "maintainers": "clickoding",
    "license": "GPL-3",
    "depends": [
        "purchase",
        "sale",
    ],
    "data": [
        "views/purchase_views.xml",
        "views/sale_views.xml",
    ]
}
